<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>PROFILE</title>
	</head>
<body bgcolor="black" text="red">
<jsp:useBean id="profile" class="servlets.GetProfile" scope="application" />
	<h1 align="center">Your Profile</h1>

		<p>Username: ${currentUser.getUsername()}</p>
		<p>Email: ${currentUser.getEmail()} </p> 
		<p>Privilege: ${currentUser.getPrivileges().getDescription()} </p><br />
		
		<pre>
			<a href='AddressDetails'>Add More Details</a>
		</pre>
		
		<p>Address Type: ${addressDetails.getAdres()} </p>
		<p>Wojewodztwo: ${addressDetails.getWojewodztwo()} </p>
		<p>City: ${addressDetails.getCity()} </p>
		<p>Postcode: ${addressDetails.getPostcode()} </p>
		<p>Street: ${addressDetails.getStreet()} </p>
		<p>House/Home number: ${addressDetails.getHouseNumber()} </p>
		  
		<pre>
			<a href='Logout.jsp'>Logout</a>
		</pre>
</body>
</html>



