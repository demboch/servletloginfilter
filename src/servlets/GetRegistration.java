package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import database.DataBase;
import domain.SetUser;
import domain.UserRegistration;
import service.BaseServlet;

@WebServlet("/GetRegistration")
public class GetRegistration extends BaseServlet {

	private static final long serialVersionUID = 1L;

	private SetUser setUser;
	private DataBase dataBase;
	private HttpSession session;
	private String message;

	public GetRegistration() {
		setUser = new SetUser();
		dataBase = new DataBase();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setAttribute("message", message);
		request.getRequestDispatcher("RegisterForm.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		UserRegistration register = setUser.checkUserRegisterForm(request);
//		Privileges privileges = Privileges.valueOf(request.getParameter("privileges"));
		
		if (register.getPassword().equals(register.getConfirmPassword())
				&& register.getEmail().equals(register.getConfirmEmail())) {

			if (dataBase.countByEmail(register.getEmail()) == 0) {
				session = request.getSession(true);
				session.setAttribute("currentUser", register);
				getUserRepository().add(register);
				dataBase.save(register);
				response.sendRedirect("UserProfile.jsp");
			} else {
				response.sendRedirect("RegisterForm.jsp");
				return;
			}

		} else {
			message = "Wrong E-mail! or Password";
			doGet(request, response);
		}
	}
}

// String password = request.getParameter("password");
// String email = request.getParameter("email");
//
// if (getUserRepository().checkRegisterEmailAndPass(password, email)) {
// if (dataBase.countByEmail(register.getEmail()) == 0) {
// session = request.getSession(true);
// session.setAttribute("currentUser", register);
// getUserRepository().add(register);
// dataBase.save(register);
// response.sendRedirect("UserProfile.jsp");
// } else {
// response.sendRedirect("UserRegistration.jsp");
// return;
// }
// } else {
// message = "Wrong E-mail! or Password";
// doGet(request, response);
// }
