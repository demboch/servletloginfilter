package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import domain.User;
import database.DataBase;
import service.BaseServlet;

@WebServlet("/GetLogin")
public class GetLogin extends BaseServlet {

	private static final long serialVersionUID = 1L;

	private HttpSession session;
	private String message;
	private User user;
	private DataBase dataBase;
	
	public GetLogin() {
		user = new User();
		dataBase = new DataBase();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setAttribute("message", message);
		request.getRequestDispatcher("UserLogin.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		try {
			if (dataBase.validate(username, password)) {
				session = request.getSession();
				user = dataBase.getByUsername(username);
				session.setAttribute("currentUser", user); // null pointer ex
				response.sendRedirect("UserProfile.jsp");
			} else {
				message = "Wrong Login or Password!!!";
				doGet(request, response);
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
}

//getUserRepository().checkLoginPassword(username, password)
/*
session.setAttribute("currentUser", user);
session.setAttribute("currentUser", user.getUsername());
session.setAttribute("email", user.getEmail());
session.setAttribute("privilege", user.getPrivilege().getId()); */