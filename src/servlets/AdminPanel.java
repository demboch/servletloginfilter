package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.DataBase;
import service.BaseServlet;

@WebServlet("/AdminPanel")
public class AdminPanel extends BaseServlet {

	private static final long serialVersionUID = 1L;

	private DataBase dataBase;
	private String message;
	
	public AdminPanel() {
		dataBase = new DataBase();
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		request.setAttribute("message", message);
		request.getRequestDispatcher("Admin.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String username = request.getParameter("username");
		String privilege = request.getParameter("privilege");

		try {
			dataBase.changePrivilege(username, privilege);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		message = "Privileges changed!!! " + privilege;
		doGet(request, response);
	}

}
