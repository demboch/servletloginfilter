package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.DataBase;
import domain.User;
import service.BaseServlet;

@WebServlet("/UsersList")
public class UsersList extends BaseServlet {
	
	private static final long serialVersionUID = 1L;
	
	private DataBase dataBase;

	public UsersList() {
		dataBase = new DataBase();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		PrintWriter out = response.getWriter();
		String premiumAccount;

		out.print("<body bgcolor='black' text='red'>"
				+ "<h1 align='center'>User List</h1>"
				+ "<table border = '1'> <tr> <td> USERNAME </td> <td> NORMAL/PREMIUM/ADMIN </td> </tr>");

		try {
			for (User user : dataBase.getUsersFromDB()) {

				premiumAccount = user.getPrivileges().getDescription();

				if (user.getPrivileges().getId() == 2 || user.getPrivileges().getId() == 3) {
					premiumAccount = user.getPrivileges().getDescription();
				}

				out.print("<tr> <td>" + user.getUsername() + "</td> <td> " + premiumAccount + " </td> </tr>");

			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		out.print("</table><br /><pre><a href='Menu.jsp'>MENU</a></pre></body>");
	}
}
