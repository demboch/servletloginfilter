package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import domain.Privileges;
import domain.User;
import service.BaseServlet;

@WebServlet("/GetProfile")
public class GetProfile extends BaseServlet {

	private static final long serialVersionUID = 1L;
	
	private User currentUser;
	private Privileges privilege;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
			
		HttpSession session = request.getSession(true);
		currentUser = (User)session.getAttribute("currentUser");	
		request.setAttribute("profile", currentUser);		
		response.sendRedirect("UserProfile.jsp");
	}

	public String getUsername(){
		return currentUser.getUsername();
	}
	
	public String getEmail(){
		return currentUser.getEmail();
	}
	
	public String getPrivileges(){
		return privilege.getDescription();
	}
}
