package service;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

public class BaseServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public void init() throws ServletException {
		if (getServletContext().getAttribute("repository") == null) {
			getServletContext().setAttribute("repository", new UserRepository());
		}
	}

	public UserRepository getUserRepository() {
		return (UserRepository) getServletContext().getAttribute("repository");
	}

}
