package service;

import java.util.HashMap;
import java.util.Map;

import domain.User;

public class UserRepository {

	private Map<String, User> users;

	public UserRepository() {
		users = new HashMap<String, User>();
	}

	public void add(User user) {
		users.put(user.getUsername(), user);
	}

	public boolean checkRegisterEmailAndPass(String password, String email) {
//		if (!users.containsKey(password) && !users.containsKey(email)) {
//			return false;
//		}
		
		if (users.get(password).getPassword().equals(password) 
				&& users.get(email).getEmail().equals(email)) {
			return true;
		}
		return false;
	}

	public boolean checkLoginPassword(String username, String password) {
		if (!users.containsKey(username)) {
			return false;
		}

		if (users.get(username).getPassword().equals(password)) {
			return true;
		}
		return false;
	}
}
