package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import domain.*;

public class DataBase {

	private final static String URL = "jdbc:mysql://localhost:3306/register";
	private final static String USER = "root";
	private final static String PASSWORD = "root";
	private final static String DRIVER = "com.mysql.jdbc.Driver";

	private Connection connection;
	private Statement statement;
	private SQLUserQuery sqlUserQuery;

	public DataBase() {
		sqlUserQuery = new SQLUserQuery();
	}

	private void runUpdate(String query) throws ClassNotFoundException, SQLException {
		Class.forName(DRIVER);
		connection = DriverManager.getConnection(URL, USER, PASSWORD);
		statement = connection.createStatement();
		statement.executeUpdate(query);
	}

	public void save(UserRegistration registration) {

		String query = sqlUserQuery.createSaveQuery(registration);

		try {
			runUpdate(query);
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public void saveAddress(UserAddressDetails addressDetails) {

		String query = sqlUserQuery.createSaveAddressQuery(addressDetails);

		try {
			runUpdate(query);
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	private int runCountQuery(String query) {
		int count = 0;

		try {
			Class.forName(DRIVER);
			connection = DriverManager.getConnection(URL, USER, PASSWORD);
			statement = connection.createStatement();
			ResultSet result = statement.executeQuery(query);

			while (result.next()) {
				count = result.getInt(1);
			}

		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}

		return count;
	}

	public int countCandidate() {
		String query = sqlUserQuery.createGetCountQuery();
		return runCountQuery(query);
	}

	public int countByEmail(String email) {
		String query = sqlUserQuery.createGetCountByEmailQuery(email);
		return runCountQuery(query);
	}

	public void changePrivilege(String username, String privilege) throws ClassNotFoundException {

		int privilegeId = 0;

		if (privilege.equals("user")) {
			privilegeId = 1;
		}

		if (privilege.equals("premium")) {
			privilegeId = 2;
		}

		if (privilege.equals("admin")) {
			privilegeId = 3;
		}

		String query = sqlUserQuery.changePrivilegesQuery(username, privilegeId);

		try {
			runUpdate(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public User getByUsername(String username) throws ClassNotFoundException {
		
		User user = new User();
		String query = sqlUserQuery.getByUsernameQuery(username);
		ResultSet result;
		
		try {
			Class.forName(DRIVER);
			connection = DriverManager.getConnection(URL, USER, PASSWORD);
			statement = connection.createStatement();
			result = statement.executeQuery(query);

			while (result.next()) {
				user.setUsername(result.getString("username"));
				user.setPassword(result.getString("password"));
				user.setEmail(result.getString("email"));
				user.setPrivileges(Privileges.values()[result.getInt("privilege")]);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return user;
	}

	public List<User> getUsersFromDB() throws ClassNotFoundException {

		List<User> users = new ArrayList<>();
		User user;
		String query = sqlUserQuery.getAllUsersQuery();
		ResultSet result;

		try {
			Class.forName(DRIVER);
			connection = DriverManager.getConnection(URL, USER, PASSWORD);
			statement = connection.createStatement();
			result = statement.executeQuery(query);

			while (result.next()) {

				user = new User();

				user.setUsername(result.getString("username"));
				user.setPassword(result.getString("password"));
				user.setEmail(result.getString("email"));
				user.setPrivileges(Privileges.values()[result.getInt("privilege")]);

				users.add(user);

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return users;

	}

	public boolean validate(String username, String password) throws ClassNotFoundException {
		boolean result = false;
		String query = sqlUserQuery.checkUserQuery(username, password);

		try {
			Class.forName(DRIVER);
			connection = DriverManager.getConnection(URL, USER, PASSWORD);
			statement = connection.createStatement();
			//runUpdate(query);
			ResultSet resultSet = statement.executeQuery(query);
			result = resultSet.next();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

}
