package database;

import domain.*;

public class SQLUserQuery {
	
	private String query = "";

	public String createSaveQuery(UserRegistration candidate) {
		query = "INSERT INTO register.users VALUES (NULL,'"+candidate.getUsername()+
				"', '"+candidate.getPassword()+"', '"+candidate.getEmail()+
				"',1);";
		
		//, '"+candidate.getPrivileges().getId() +"'
        return query;
    }
	
	public String createSaveAddressQuery(UserAddressDetails addressDetails) {
		query = "INSERT INTO register.address VALUES (NULL,'"+addressDetails.getAdres()+
				"', '"+addressDetails.getWojewodztwo()+"', '"+addressDetails.getCity()+
				"', '"+addressDetails.getPostcode()+"', '"+addressDetails.getStreet()+
				"', '"+addressDetails.getHouseNumber()+"');";

        return query;
    }
	
	public String createGetCountQuery() {
		query = "SELECT COUNT(id) AS count FROM register.users;";
		return query;
	}
	
	public String createGetCountByEmailQuery(String email) {
		query = "SELECT count(*) FROM register.users where email='"+ email +"';";
		return query;
	}
    
	public String getAllUsersQuery() {	
		query = "SELECT * FROM register.users";
		return query;
	}
	
	public String changePrivilegesQuery(String username, int privilegeId) {
		query = "UPDATE register.users SET privilege = " + privilegeId + " where username = '"+ username + "'";
		return query;
	}
	
	public String getByUsernameQuery(String username) {
		query = "SELECT * FROM register.users WHERE username = '"+ username +"'";
		return query;
	}
	
	public String checkUserQuery(String username, String password) {
		query = "SELECT * FROM register.users WHERE username = '"+ username +"' AND password = '"+password+"'";
		return query;
	}

}
