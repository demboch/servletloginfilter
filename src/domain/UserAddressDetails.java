package domain;

public class UserAddressDetails {

	private String addressType;
	private String wojewodztwo;
	private String city;
	private String postcode;
	private String street;
	private String houseNumber;
	
	public String getAdres() {
		return addressType;
	}
	public void setAdres(String addressType) {
		this.addressType = addressType;
	}
	public String getWojewodztwo() {
		return wojewodztwo;
	}
	public void setWojewodztwo(String wojewodztwo) {
		this.wojewodztwo = wojewodztwo;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getPostcode() {
		return postcode;
	}
	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getHouseNumber() {
		return houseNumber;
	}
	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}
	
}
