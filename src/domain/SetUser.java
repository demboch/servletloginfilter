package domain;

import javax.servlet.http.HttpServletRequest;

public class SetUser {

	public UserRegistration checkUserRegisterForm(HttpServletRequest request) {

		UserRegistration register = new UserRegistration();

		register.setUsername(request.getParameter("username"));
		register.setPassword(request.getParameter("password"));
		register.setConfirmPassword(request.getParameter("confirmPassword"));
		register.setEmail(request.getParameter("email"));
		register.setConfirmEmail(request.getParameter("confirmEmail"));
		register.setPrivileges(Privileges.USER);

		return register;
	}

	public UserAddressDetails checkUserAddressDetailsForm(HttpServletRequest request) {

		UserAddressDetails addressDetils = new UserAddressDetails();

		addressDetils.setAdres(request.getParameter("addressType"));
		addressDetils.setWojewodztwo(request.getParameter("wojewodztwo"));
		addressDetils.setCity(request.getParameter("city"));
		addressDetils.setPostcode(request.getParameter("postcode"));
		addressDetils.setStreet(request.getParameter("street"));
		addressDetils.setHouseNumber(request.getParameter("houseNumber"));

		return addressDetils;
	}

}
