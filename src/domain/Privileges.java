package domain;

public enum Privileges {
	GUEST(0,"Guset"), USER(1,"Normal Account"), PREMIUM(2,"Premium Account"), ADMIN(3, "Admin");
	
	private int id;
	private String description;	
	
	Privileges(int id, String description) {
		this.id = id;
		this.description = description;
	}

	public int getId() {
		return id;
	}

	public String getDescription() {
		return description;
	}

	public void setId(int id) {
		this.id = id;
	}	
}