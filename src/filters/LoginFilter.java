package filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter(filterName = "LoginFilter")
public class LoginFilter implements Filter {

	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
			throws ServletException, IOException {
		HttpSession httpSession = ((HttpServletRequest) req).getSession();
		HttpServletResponse response = (HttpServletResponse) resp;
		if (httpSession.getAttribute("username") != null) {
			httpSession.invalidate();
			response.sendRedirect("UserProfile.jsp");
			return;
		}

		chain.doFilter(req, resp);
	}

	public void init(FilterConfig config) throws ServletException {

	}

	public void destroy() {
	}

}