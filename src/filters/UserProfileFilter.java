package filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter(filterName = "UserProfileFilter")
public class UserProfileFilter implements Filter {

	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
			throws ServletException, IOException {
		HttpServletResponse response = (HttpServletResponse) resp;
		HttpSession httpSession = ((HttpServletRequest) req).getSession();

		if (httpSession.getAttribute("username") == null) {
			response.sendRedirect("UserLogin.jsp");
			return;
		}
		chain.doFilter(req, resp);
	}

	public void init(FilterConfig config) throws ServletException {

	}

	public void destroy() {
	}

}